inline int powInt(int x, int y)
{
    for (int i = 0; i < y; i++)
    {
        x *= 10;
    }
    return x;
}

inline size_t parseInt(std::vector<int> &v)
{
    size_t sum{0};
    int len{static_cast<int>(v.size())};
    for (int i = 0; i < len; i++)
    {
        int n{v.at(len - (i + 1)) - '0'};
        sum += powInt(n, i);
    }
    return sum;
}

inline bool fileExists(const std::string &fname)
{
    if (FILE *file = fopen(fname.c_str(), "r"))
    {
        fclose(file);
        return true;
    }
    else
    {
        return false;
    }
}

void saveTransferMatrix(const std::string &fname,
                        const std::shared_ptr<duneuro::DenseMatrix<double>> tMat)
{
    std::ofstream fout(fname, std::ios::binary);
    fout << "::" << tMat->rows() << "::" << tMat->cols() << "::";
    fout.write(reinterpret_cast<char *>(tMat->data()),
               tMat->rows() * tMat->cols() * sizeof(tMat->data()[0]));
    fout.close();
}

std::shared_ptr<duneuro::DenseMatrix<double>> readTransferMatrix(const std::string &fname)
{
    std::ifstream fin{fname, std::ios::binary};

    //read header
    char separatorChar = ':';
    char h1, h2;
    fin.read(&h1, sizeof(h1));
    fin.read(&h2, sizeof(h2));
    if (!(h1 == separatorChar && h2 == separatorChar))
    {
        printf("\nSomething went wrong while reading the binary file.\n");
        return 0;
    }
    std::vector<int> nRowsVec;
    std::vector<int> nColsVec;
    char digit;
    fin.read(&digit, sizeof(digit));
    while (digit != separatorChar)
    {
        nRowsVec.push_back(static_cast<int>(digit));
        fin.read(&digit, sizeof(digit));
    }
    fin.read(&h2, 1);
    if (h2 != separatorChar)
    {
        printf("\nSomething went wrong while reading the binary file.\n");
        return 0;
    }
    fin.read(&digit, sizeof(digit));
    while (digit != separatorChar)
    {
        nColsVec.push_back(static_cast<int>(digit));
        fin.read(&digit, sizeof(digit));
    }
    fin.read(&h1, 1);
    size_t nRows{parseInt(nRowsVec)};
    size_t nCols{parseInt(nColsVec)};
    //printf("\nnumber of rows: %d\n", nRows);
    //printf("\nnumber of cols: %d\n", nCols);

    //now read the data
    static std::shared_ptr<duneuro::DenseMatrix<double>> matrixTransfer(new duneuro::DenseMatrix<double>(nRows, nCols, 0.));
    fin.read(reinterpret_cast<char *>(matrixTransfer->data()), nRows * nCols * sizeof(matrixTransfer->data()[0]));
    fin.close();
    return matrixTransfer;
}

void saveLFfile(const std::string &fname,
            std::vector<std::vector<double>> &num_transfer,
            const int numDipoles, 
            const int numElectrodes,
            bool saveAsText=false)
{
    //save leadfields as binary and text files
    std::ofstream fidBin(fname, std::ios::binary);
    fidBin << "::" << numDipoles << "::" << numElectrodes << "::";
    for (int i = 0; i < numDipoles; ++i)
    {
        fidBin.write(reinterpret_cast<char *>(&num_transfer[i][0]), numElectrodes * sizeof(num_transfer[i][0]));
    }
    fidBin.close();
    
    if (saveAsText)
    {
        std::ofstream fidTxt{fname + ".txt"};
        for (unsigned int i = 0; i < numDipoles; ++i)
        {
            for (unsigned int j = 0; j < numElectrodes; ++j)
                fidTxt << num_transfer[i][j] << " ";
            fidTxt << "\n";
        }
        fidTxt.close();
    }
    
}
